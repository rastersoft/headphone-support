# Headphones support with "wireless" charging

This is the design of a headphone support which, with some extra hardware,
allows to charge bluetooth headphones while hanged in there.

There are two designs: one for "horizontal support" and another for "vertical
support".

![Design in FreeCAD](3d_design.png)

![Dimensions](dimensions.png)

## RealWorld(tm) results

![real world](final_support.jpg)
